import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:app_usage/app_usage.dart';
import 'package:device_info/device_info.dart';
import 'package:http/http.dart' as http;
import 'package:device_apps/device_apps.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AppUsage appUsage = new AppUsage();
  var apps = new Map();
  var System= new Map();
  String data ='';
  Timer timer;
  @override
  void initState() {
    super.initState();
    initPlatformState();
    timer=Timer.periodic(Duration(seconds: 7), (Timer t) => getUsageStats());
  }


  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
  }

  void getUsageStats() async {
    DateTime endDate = new DateTime.now();
    DateTime startDate = DateTime(endDate.year, endDate.month, endDate.day, 0, 0, 0);
    Map<String, dynamic> usage = await appUsage.getUsage(startDate, endDate);
    usage.removeWhere((key,val) => val == 0);
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    // usage['System'] = androidInfo.model;
    setState(() => apps = makeString(usage));
    final now = new DateTime.now();
    String formatter = DateFormat('yMd').format(now);



    Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    System={"SystemID":androidInfo.androidId,'Apps':apps,'Model':androidInfo.model,'Type':'Android','LiveLocation':[position],'Report Date':formatter};
    print(System);
    setState(() => data =androidInfo.model);
    final String url = 'http://192.168.1.106:5000/';
    final response = await http.post(url,body: JsonEncoder().convert(System));
    print(response.statusCode);
    print("Posting To Server Successfull:${url}");

  }

  Map<dynamic,dynamic> makeString(Map<String, dynamic> usage) {
    String result = '';
    double StartTime=0;
    var Information = new Map();
    usage.forEach((k,v) {
      print(k);
      String appName = k.split('.').last;

      String TotaltimeInMins = (v / 60).toStringAsFixed(2);
      if (StartTime == 0){
        StartTime=double.parse(TotaltimeInMins);
      }
      double UsageTime = double.parse(TotaltimeInMins)-StartTime ;
      Information[appName]={  "Total Time Usage":TotaltimeInMins,'UsageTime':UsageTime};

    }

    );
    return Information;
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(

        body:
          Center(

            child: Container(
                constraints: BoxConstraints.expand(),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage("https://icons-for-free.com/iconfiles/png/512/r2d2+robot+starwars+icon-1320166698566079188.png"),
                        fit: BoxFit.cover)
                ),
                child: Center(child: Text('',
                  textAlign: TextAlign.center, style:
                  TextStyle(color: Colors.brown, fontSize: 25, fontWeight: FontWeight.bold),),)
            )
          ),
        //Text(
        //   data,
        //   overflow: TextOverflow.ellipsis,
        //   style: TextStyle(
        //     fontSize: 20.0, // insert your font size here
        //   ),
        // ),


        floatingActionButton: FloatingActionButton(
            onPressed: getUsageStats,
            child: Icon(Icons.cached)
        ),
      ),
    );
  }
}