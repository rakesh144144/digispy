﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiSpyAccessControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window

    {

        public MainWindow()

        {

            InitializeComponent();


        }



        private void btnLogin_Click(object sender, RoutedEventArgs e)

        {
            if (txtUsername.Text =="Admin" && txtPassword.Password == "Admin")

                {

                    MainPageDashBoard db = new MainPageDashBoard();
                    db.Show();
                    this.Close();

            }

                else

                {

                    MessageBox.Show("Username or password is incorrect", "", MessageBoxButton.OK, MessageBoxImage.Information);

                }

        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)

        {

            DragMove();

        }



        private void btnExit_Click(object sender, RoutedEventArgs e)

        {

            Application.Current.Shutdown();

        }

        private void txtUsername_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }

}
